# Näytön selitys

Tarkoituksena on tehdä tekstipohjaisen roolipelin pelimoottori. Luova kirjoittaminen on minun yksi harrastus, joten olen jo pitkään tahtonut yhdistää sen osaamiseen. Tässä näytössä kuitenkin teen vain sen moottorin, sillä hyvän tarinan kirjoittamisessa kestäisi liian kauan. Moottorin tarkoitus on yksinkertaistaa pelin tekemistä, luomalla valmiita funktioita joita yhdistän yksinkertaiseen käyttöliittymään, jotta tulevaisuudessa voi keskittyä vain tarinan kirjoittamiseen.

Kommentteja saattaa olla sekä englanniksi että suomeksi. Mun pää vaihtaa kielien välillä kuin vivusta riippuen mitä kieltä oon käyttänyt juuri, ja tekstiä tulee sen mukaan automaattisesti.

# Aloitus -- TÄRKEÄ

<details open>
    <summary>
    Piilota/laajenna
    </summary>
    <br />

## Tarvittavat kirjastot

Palvelimella täytyy olla seuraavat PHP laajennukset toimiakseen:

-   **php-json**
-   **php-mysql**
-   **php-mbstring**

Varmista php.ini tiedostosta, että seuraavat laajennukset ovat päällä:

-   **mbstring**
-   **pdo_mysql**

## Tiedostojen lisääminen

Esimerkki kansio struktuuri:

```
var
└── www
    └── rpg
        ├── .key <--- EI TULE VALMIINA
        ├── MySQL.ini <--- EI TULE VALMIINA
        └── public_html
            ├── bootstrap
            ├── css
            ├── editor
            │   ├── resources
            │   └── src
            │       ├── ckeditor
            │       ├── js
            │       └── php
            ├── pages
            │   └── chapter_1
            │       ├── intro.html
            │       └── intro.json
            ├── src
            │   ├── js
            │   └── php
            ├── index.php
            ├── keyGen.php
            ├── README.md
            └── rpg.sql
```

Me laitamme muutaman tiedoston rootin yläpuolelle, eli rpg/ kansioon

### Sodium avain

Sivustolla on tiedosto nimeltä keyGen.php. Tämä luo tarvittavan .key tiedoston public_html/ kansioon. Siirrä tämä rpg/ kansioon.
keyGen.php kannattaa poistaa, kun olet varmistanut, että peli avautuu, ja gameData keksi on olemassa enkryptattuna.

### rpg.sql

Ennenkuin pyörität skriptin palvelimella, kannattaa käydä skriptin sisällä vaihtamassa valmiin käyttäjän salasana vähintään. Se löytyy riviltä 52.

```
CREATE USER 'Dungeon_master' IDENTIFIED BY 'Correct-Horse-Battery-Staple';
```

Vaihda Correct-Horse-Battery-Staple johonkin muuhun. Käyttäjä nimenkin voi vaihtaa mikäli tahdot.

### MySQL.ini

Document rootin yläpuolelle pitää luoda MySQL.ini tiedosto. Tässä on SQL skriptin luoman käyttäjän tiedot, joten ne pitää vaihtaa omiisi.

```
[MySQL]
server = "127.0.0.1
username = "Dungeon_master"
password = "Correct-Horse-Battery-Staple"
database = "rpg"
```

</details>
<br />

# Editorin käyttö

<details open>
    <summary>
    Piilota/laajenna
    </summary>
    <br />
    
Oletuksena, sivun admin käyttäjälle on samat tunnukset kuin MySQL käyttäjällä.

## **!!! Varoitus !!!**

Tässä vaiheessa ei vielä pysty linkkaamaan ensimmäistä kappaletta seuraavaan, joten peliä pitää testata vain ensimmäisen kappaleen sisällä. Periaattessahan, koko pelin voi tehdä vaikka pelkästään yhdellä kappaleella, mutta jos tulee pitkä peli, voi olla aika hankala navigoida jossain vaiheessa. Kappaleita ei myöskään voi vielä asettaa loppumaan odottamaan seuraavan kappaleen valmistumista. **Nämä ovat 2/3 toiminnoista joka estävät minua kutsumasta tätä valmiiksi sovellukseksi.** Kaikki muu on vain korjailua ja parantelua enintään.

# Sivun luonti

Valitse sivuvalikosta 'New Page'. Anna sivulle kuvaava otsikko navigaatiota varten, ja ala kirjoittamaan.

## Sivun ominaisuudet

Täältä löytyy kaikki mitä pelillisesti voisi kutsua pelimekaanikoiksi. Tärkein asia mikä kuitenkin aina pitää käydä laittamassa sivua luodessa, on **page links**, muuten pelaaja joutuu jumiin kyseiselle sivulle.

### Choices

Jos tahdot pelille monta eri reittiä missä tahansa pelissä, helpoin tapa on tehdä se valinnoilla. Kun painat **add a choice** nappia, laatikkoon ilmestyy uusi tekstikenttä. Tähän kirjoitetaan valintanapin teksti. Valintojen linkit määrittyvät järjestyksessä. Jos teet kolme valintaa, **page links** kohdan alta löytyy kolme linkki valintaa. Ensimmäinen linkki yhdistyy ensimmäiseen valintaan, toinen toiseen jne.

### Stat tools

Vaihtoehtoisesti sivulle voi laittaa tarkistuksia pelaajan attribuuteille. Toistaiseksi peli käyttää Dungeons & Dragons järjestelmää, mutta tulevaisuudessa ajattelin että tämä olisi muokattavissa. Sivun voi myös asettaa antamaan pelaajalle attribuutti pisteitä niin monta kuin tahdot. D&D:n mukaisesti, suurin attribuutin piste määrä on 20 (tällä hetkellä pelaajan on mahdollista element inspectorin kautta lisätä maksimi määrää. Tämä TODO listassa), joten pisteitä kannattaa antaa se mielessä.

Jos lisäät tarkistuksen, ilmestyy teksikenttä ja valinta laatikko. Teksikenttään laitetaan vaatimustaso, ja valinnasta valitaan haluttu attribuutti. Lisäksi ilmestyy uusi kohta **text on failure**, jossa on checkboxi ja tekstikenttä. Mielestäni, ei ole hyvää tarinan kerrontaa jos pelaajaa rankaistaan välittömästi häviöllä jos hänellä ei ole tarpeeksi voimaa tai älykkyyttä yms., jotenka, "game over" on vain vaihtoehtoinen. Tekstikenttään kirjoitetaan sitten mitä tapahtuu kun pelaaja ei pääse testiä lävitse.

Linkit pysyvät samana vaikka pelaaja epäonnistuu, ellei peli mene "game over" tilaan, jolloinka pelaajalle annetaan mahdollisuus mennä tallennus pisteeseen, tai aloitta peli alusta.

### Page checkpoint

Tämä checkbox yksinkertaisesti asettaa sivun tallennuspisteeksi. Älä käytä "game overia" samalla sivulla jossa on tallennuspiste. Estän tämän vielä koodissa jossain vaiheessa.

## Pelin ominaisuudet

Tässä on se kolmas toiminto joka puuttuu. Täältä pystyisi asettamaan lippuja, joita sitten pystytään käyttämään "muistina" pelaajan valinnoista ja niiden seurauksista. Pelaaja voisi esim. tehdä valinnan johka hänellä ei ollut tarpeeksi attribuutteja, joka johti jonkin toisen hahmon kuolemaan. Tämä tapahtuma asettaisi lipun, jota myöhemmin voidaan käyttää.

Tästä pystyisi myös sitten asettaamaan attribuutit jota peli käyttää. On monta hienoa pöytä roolipeli systeemiä josta vetää inspiraatiota, ja kaikki niistä ei käytä nimen oman D&D:n attribuutteja.

</details>
