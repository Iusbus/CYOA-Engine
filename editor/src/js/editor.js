"use strict";

const ajax = new XMLHttpRequest();
var editors = [];

function createEditor(elementId) {
    if (typeof editors[elementId] === "undefined") {
        return InlineEditor.create(document.querySelector("#" + elementId), {
            toolbar: {
                items: ["heading", "|", "bold", "blockQuote", "italic", "|", "indent", "outdent", "|", "undo", "redo", "|", "link"],
            },
        })
            .then(function (editor) {
                editors[elementId] = editor;
            })
            ["catch"](function (err) {
                return console.error(err);
            });
    }
}

$(document).ready(function () {
    createEditor("mainEditor");
    createEditor("failText");
});

document.querySelector("#submit").addEventListener("click", function () {
    var ch = Number(document.getElementById("chSel").value);
    var pageTitle = document.getElementById("title").value;
    var curPage = document.getElementById("filter").value;
    var editorData = encodeURIComponent(editors.mainEditor.getData());
    ajax.open("POST", "src/php/editor.php", true);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("chapter=" + ch + "&title=" + curPage + "&txt=" + editorData + "&newTitle=" + pageTitle + "&JSON=" + getProperties());

    ajax.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            pageTitle != curPage && curPage != "New Page" && curPage != undefined ? (db.delete(ch, curPage), db.set(ch, pageTitle)) : db.set(ch, pageTitle);
            prepSel();
            clearProperties(true);
            $("#alert").fadeIn(500);
            setTimeout(function () {
                $("#alert").fadeOut(500);
            }, 3000);
            document.getElementById("filter").disabled = false;
            document.getElementById("chSel").disabled = false;
        }
    };
});
