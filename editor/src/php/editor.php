<?php 
$ini = parse_ini_file($_SERVER["DOCUMENT_ROOT"].'/../MySQL.ini');
$host = $ini['server'];
$db   = $ini['database'];
$user = $ini['username'];
$pass = $ini['password'];

$dsn = "mysql:host=$host;dbname=$db;charset=utf8mb4";

$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];

try {
    $pdo = new PDO($dsn, $user, $pass, $options);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

$insert = $pdo->prepare('INSERT INTO pageIndex (`chapter`,`title`) VALUES (?,?)');
$delete = $pdo->prepare('DELETE FROM pageIndex WHERE (chapter=? AND title=?)');

$pageRoot = $_SERVER['DOCUMENT_ROOT']."/pages";


if (isset($_POST['txt']) && isset($_POST['title']) && isset($_POST['newTitle'])) {
    $txt = $_POST['txt'];
    $json = $_POST['JSON'];
    $oldTitle = $_POST['title'];
    $newTitle = $_POST['newTitle'];
    $ch = $_POST['chapter'];
    
    $chapterDir = $pageRoot."/chapter_".$ch.'/';
    if (!file_exists($chapterDir)) {
        mkdir($chapterDir,0777);
    }   
    //Tiedosto nimien kannattaa olla ilman välejä ja ilman isoja kirjaimia
    $oldFile = strtolower(str_replace(' ','_',$oldTitle));
    $newFile = strtolower(str_replace(' ','_',$newTitle));
    
    //intro pitää löytyä, hard koodattu pelin alku niin.
    if($ch==1){
        file_exists("$pageRoot/chapter_$ch/intro.html") ? $newFile = $newFile : $newFile = 'intro' & $newTitle = "Intro";
    }


    $txtFile = fopen("$pageRoot/chapter_$ch/$newFile.html",'w');
    $jsonFile = fopen("$pageRoot/chapter_$ch/$newFile.json","w");

    if ($oldFile != $newFile && $oldFile != 'intro' && $oldFile != 'new_page') { //Rename ei toiminut
        unlink("$pageRoot/chapter_$ch/$oldFile.html"); 
        unlink("$pageRoot/chapter_$ch/$oldFile.json");
        $delete->execute([$ch,$oldTitle]);
        $insert->execute([$ch,$newTitle]);
    }

    fwrite($txtFile,$txt);
    fwrite($jsonFile,$json);
    if($oldFile == 'new_page'){
        $insert->execute([$ch,$_POST['newTitle']]);
    }
}

if (isset($_POST['q']) && $_POST['q'] != 'page') {
    $q = strtolower(str_replace(' ','_',$_POST['q']));
    $ch = $_POST['chapter'];
    if(file_exists("$pageRoot/chapter_$ch/$q.html") && $q != 'new_page'){
        $txtFile = file_get_contents("$pageRoot/chapter_$ch/$q.html");
        $jsonFile = file_get_contents("$pageRoot/chapter_$ch/$q.json");
        echo json_encode(array($txtFile,$jsonFile));
    } else {
        if ($q == 'new_page'){
            echo '';
        } else {
            echo '404';
        }
    }
}

$refresh = $pdo->prepare('SELECT chapter,title FROM pageIndex ORDER BY chapter ASC');
if (isset($_POST['refresh'])) {
    $json = [];
    $refresh->execute();
    while ($row = $refresh->fetch()) {
        #isset($json[$row['chapter']]) ? $json[$row['chapter']] += $row : $json[$row['chapter']] = $row;
        array_push($json,$row);
    }
    echo json_encode($json);
};
?>