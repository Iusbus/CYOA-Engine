"use strict";

document.getElementById("userBtn").addEventListener("click", (e) => {
    if (e.target.innerHTML == "Login") {
        var element = document.querySelector(".login-form").style;
        element.display = "block";
        element.opacity = 1;
        document.getElementById("loginName").focus();
    } else {
        document.querySelector(".custom-tooltip").classList.toggle("tooltip-visible");
    }
});
document.querySelector(".login-form").addEventListener(
    "click",
    (e) => {
        if (e.target !== e.currentTarget) {
            return;
        } else {
            e.target.style.opacity = 0;
            e.target.style.display = "none";
        }
    },
    true
);
document
    .querySelector(".custom-tooltip")
    .querySelectorAll("button")
    .forEach((button) => {
        switch (button.innerHTML) {
            case "Logout":
                button.addEventListener("click", () => {
                    document.cookie = "userData=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
                    window.location.reload();
                });
                break;
            case "Change password":
                button.addEventListener("click", () => {
                    //TODO: This. The PHP code is ready, just do the JS
                    alert("Not yet implented, sorry");
                });
            default:
                break;
        }
        button.addEventListener("click", (e) => {});
    });

var login = document.querySelector(".user-login");
var create = document.querySelector(".user-create");
function formSwitch() {
    if (getComputedStyle(login).opacity == 1) {
        login.style.opacity = 0;
    } else {
        create.style.opacity = 0;
    }
}
login.addEventListener("transitionend", (e) => {
    if (e.target != login) {
        return;
    }
    if (getComputedStyle(login).opacity == 1) {
        return;
    } else {
        create.style.opacity = "1";
        create.style.display = "block";
        login.style.display = "none";
    }
});
create.addEventListener("transitionend", (e) => {
    if (e.target != create) {
        return;
    }
    if (create.style.opacity == 1) {
        return;
    } else {
        login.style.opacity = "1";
        login.style.display = "block";
        create.style.display = "none";
    }
});

document.getElementById("userPass").addEventListener("keyup", (e) => {
    if (e.code == "TAB") {
        return;
    } else if (e.code == "Enter") {
        document.getElementById("userSubmit").click();
    }
    if (e.target.value != document.getElementById("confPass").value && document.getElementById("confPass").value != "") {
        document.getElementById("userSubmit").disabled = true;
        document.getElementById("userSubmit").innerHTML = "Passwords don't match";
        document.getElementById("userSubmit").classList.add("btn-danger");
        document.getElementById("userSubmit").classList.remove("btn-custom");
    } else {
        document.getElementById("userSubmit").classList.add("btn-custom");
        document.getElementById("userSubmit").classList.remove("btn-danger");
        if (document.getElementById("userName").value != "") {
            document.getElementById("userSubmit").innerHTML = "Create user";
            document.getElementById("userSubmit").disabled = false;
        } else {
            document.getElementById("userSubmit").innerHTML = "Missing username";
        }
    }
});
document.getElementById("confPass").addEventListener("keyup", (e) => {
    if (e.code == "TAB") {
        return;
    } else if (e.code == "Enter") {
        document.getElementById("userSubmit").click();
    }
    if (e.target.value != document.getElementById("userPass").value) {
        document.getElementById("userSubmit").disabled = true;
        document.getElementById("userSubmit").innerHTML = "Passwords don't match";
        document.getElementById("userSubmit").classList.add("btn-danger");
        document.getElementById("userSubmit").classList.remove("btn-custom");
    } else {
        document.getElementById("userSubmit").classList.remove("btn-danger");
        document.getElementById("userSubmit").classList.add("btn-custom");

        if (document.getElementById("userName").value != "") {
            document.getElementById("userSubmit").innerHTML = "Create user";
            document.getElementById("userSubmit").disabled = false;
        } else {
            document.getElementById("userSubmit").innerHTML = "Missing username";
        }
    }
});

document.getElementById("loginSubmit").addEventListener("click", () => {
    var username = document.getElementById("loginName");
    var password = document.getElementById("loginPass");

    ajax.open("POST", "src/php/MySQL.php");
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("username=" + username.value + "&password=" + password.value + "&loginSubmit=" + true);

    ajax.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText == "Success") {
                window.location.reload();
                document.querySelector(".toggleableText").style.display = "none";
            } else {
                username.style.border = "red 1px solid";
                password.style.border = "red 1px solid";
                document.querySelector(".toggleableText").style.display = "block";
            }
        }
    };
});

document.getElementById("userSubmit").addEventListener("click", () => {
    var username = document.getElementById("userName");
    var password = document.getElementById("userPass");
    var confirm = document.getElementById("confPass");

    ajax.open("POST", "src/php/MySQL.php");
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send("username=" + username.value + "&password=" + password.value + "&confirm=" + confirm.value + "&userSubmit=" + true);

    ajax.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText == "Success") {
                window.location.reload();
            } else {
                console.log("failure"); // TODO: User feedback
            }
        }
    };
});
