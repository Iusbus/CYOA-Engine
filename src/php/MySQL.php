<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/src/php/encryptor.php';
$ini = parse_ini_file($_SERVER["DOCUMENT_ROOT"].'/../MySQL.ini');
$host = $ini['server'];
$db   = $ini['database'];
$user = $ini['username'];
$pass = $ini['password'];

$dsn = "mysql:host=$host;dbname=$db;charset=utf8mb4";

$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];

try {
    $pdo = new PDO($dsn, $user, $pass, $options);
} catch (\PDOException $e) {
    throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

$newUser = $pdo->prepare('INSERT INTO users (`uName`,`uPass`,`saveData`,`userLevel`) VALUES (?,?,?,?)');
$changePass = $pdo -> prepare('UPDATE users SET uPass=? WHERE uName=?');
$getUser = $pdo->prepare('SELECT * FROM users WHERE uName=?');

if (isset($_POST['username'],$_POST['password'],$_POST['loginSubmit']) && !isset($_COOKIE['userData'])) {
    $getUser->execute([$_POST['username']]);
    $user = $getUser->fetch();
    if (password_verify($_POST['password'],$user['uPass'])) {
        setcookie('userData',safeEncrypt(json_encode([$user['uName'],$user['saveData'],$user['userLevel']]),$key),$time,'/');
        echo 'Success';
    } else {
        echo 'False';
    }
    exit();
}

if (isset($_POST['username'],$_POST['password'],$_POST['confirm'],$_POST['userSubmit']) && !isset($_COOKIE['userData'])) {
    if($_POST['password'] != $_POST['confirm']) {
        //This is really only possible if the user delibirately tries to break it
        echo 'Fail';
        exit();
    }
    $newUser->execute([$_POST['username'],password_hash($_POST['password'],PASSWORD_DEFAULT),$_COOKIE['gameData'],1]);
    setcookie('userData',safeEncrypt(json_encode([$_POST['username'],$_COOKIE['gameData'],1]),$key),$time,'/');
    exit();
}

if (isset($_POST['oldPass'],$_POST['newPass'],$_POST['chPass'],$_COOKIE['userData'])) {
    $getUser->execute([$decrypted[0]]);
    $user = $getUser->fetch();
    if (password_verify($_POST['password'],$user['uPass'])) {
        $changePass->execute([$decrypted[0],password_hash($_POST['newPass'],PASSWORD_DEFAULT)]);
        echo 'Success';
    } else {
        echo 'Fail';
    }
    exit();
}
?>