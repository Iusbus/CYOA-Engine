<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/src/php/encryptor.php';

class cookie {
    function default() {
        global $time,$key;
        $arr = array(
            'chapter'=>1,
            'page'=>'intro',
            'stats'=>array(
                'strength'=>1,
                'dexterity'=>1,
                'constitution'=>1,
                'intelligence'=>1,
                'wisdom'=>1,
                'charisma'=>1,
                'unspent'=>0
            ),
            'checkpointStats'=>array(
                'strength'=>1,
                'dexterity'=>1,
                'constitution'=>1,
                'intelligence'=>1,
                'wisdom'=>1,
                'charisma'=>1,
                'unspent'=>0
            ),
            'flags'=>array() // Käyttämätön näyttöä varten, sillä tämän tekeminen ei näyttäisi uutta osaamista mielestäni
        );
        $val = safeEncrypt(json_encode($arr),$key);
        setcookie('gameData',$val,$time,'/');
    }
    function refresh() {
        global $time;
        setcookie('gameData',$_COOKIE['gameData'],$time,'/');
        if (isset($_COOKIE['userData'])) {
            setcookie('userData',$_COOKIE['userData'],$time,'/');
        }
    }
}
$cookie = new cookie();
if(!isset($_COOKIE['gameData'])){
    $cookie->default();
} else {
    $cookie->refresh();
}
if(!isset($_COOKIE['gameData'])){
    sleep(2);
} else {
    $decrypted = json_decode(safeDecrypt($_COOKIE['gameData'],$key),true);
}
?>